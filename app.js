let express = require("express")
let session = require("express-session");
let cors = require("cors");//跨域解决方案
let bodyParser = require("body-parser");//处理post请求
let fs = require("fs");
let app = express();
//中间件解决编码问题
const mw = (req, res, next) => {
    console.log("中间件")
    res.setHeader("Content-type", "text/html;charset=utf-8");
    next()
}
app.use(mw);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
// 解决跨域
app.use(cors());
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
}))
//把各个路由注册进来,作业 自己思考下怎么自动注册进来
// app.use("/user", require("./router/user"))
// app.use("/goods", require("./router/goods"))
// 遍历路由目录,自动注入路由文件
let dirs = fs.readdirSync("./router");
for (const filename of dirs) {
    if (filename != "baserouter.js") {
        let pathname = filename.replace(".js", "")
        console.log(pathname);
        app.use("/" + pathname, require("./router/" + pathname));
    }
}
app.listen(8080)


