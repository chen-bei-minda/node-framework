const config = require("../config/db");
let { Sequelize } = require("sequelize");

const sequelize = new Sequelize(config.database, config.user, config.password, {
    host: config.host,
    dialect: 'mysql',
    define: {
        underscored: true,
    }
});

module.exports = sequelize;