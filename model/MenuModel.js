const { DataTypes, Model } = require('sequelize');

let sequelize = require("./BaseModel");

class Menu extends Model { }

Menu.init({
    menuId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    img: {
        type: DataTypes.STRING,
        allowNull: false,
    }
}, {
    // 这是其他模型参数
    sequelize, // 我们需要传递连接实例
    modelName: 'Menu', // 我们需要选择模型名称
    tableName: 'Menu',
});

// 定义的模型是类本身

Menu.sync();

module.exports = Menu;